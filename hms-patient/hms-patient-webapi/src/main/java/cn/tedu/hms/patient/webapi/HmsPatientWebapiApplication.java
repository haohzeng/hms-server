package cn.tedu.hms.patient.webapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HmsPatientWebapiApplication {

    public static void main(String[] args) {
        SpringApplication.run(HmsPatientWebapiApplication.class, args);
    }

}
