package cn.tedu.hms.patient.webapi.mapper;


import cn.tedu.hms.pojo.vo.PatientVO;
import org.springframework.stereotype.Repository;

@Repository
public interface PatientMapper {

    int updatePersonalInfo();
}
