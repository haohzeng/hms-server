package cn.tedu.hms.patient.webapi.service.impl;

import cn.tedu.hms.common.ex.ServiceException;
import cn.tedu.hms.common.web.State;
import cn.tedu.hms.patient.service.IPatientService;
import cn.tedu.hms.patient.webapi.mapper.PatientMapper;

import cn.tedu.hms.pojo.vo.PatientVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class PatientServiceImpl implements IPatientService {
    @Autowired
    PatientMapper patientMapper;

    }

    @Override
    public void updatePersonalInformational(PatientVO patientVO) {

    }
}
