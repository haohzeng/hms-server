package cn.tedu.hms.pojo.entity;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class User implements Serializable {
    private Long id;                            // bigint unsigned auto_increment primary key,
    private String username;                    // varchar(50)                null comment '用户名',
    private String password;                    // char(64)                   null comment '密码（密文）',
    private String idCard;                      // char(20)                   null comment '身份证号',
    private String phone;                       // varchar(50)                null comment '手机号码',
    private String email;                       // varchar(50)                null comment '电子邮箱',
    private String lastLoginIp;                 // varchar(50)                null comment '最后登录IP地址（冗余）',
    private LocalDateTime gmtLastLogin;         // datetime                   null comment '最后登录时间（冗余）',
    private LocalDateTime gmtCreate;            // datetime                   null comment '数据创建时间',
    private LocalDateTime gmtModified;          // datetime                   null comment '数据最后修改时间',
}
