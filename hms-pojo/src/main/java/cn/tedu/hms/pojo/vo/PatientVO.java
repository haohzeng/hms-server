package cn.tedu.hms.pojo.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class PatientVO implements Serializable {
    private String name;
    private String gender;
    private String nativePlace;
    private String birthPlace;
    private String username;
    private String nation;
    private String phone;
    private String IDCard;
    private String password;
    private Integer reserved;
    private String medicalRecord;
}
