package cn.tedu.hms.pojo.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserDTO implements Serializable {
    private String username;                    // varchar(50)                null comment '用户名',
    private String password;                    // char(64)                   null comment '密码（密文）',
    private String idCard;                      // char(20)                   null comment '身份证号',
    private String phone;                       // varchar(50)                null comment '手机号码',
    private String email;                       // varchar(50)                null comment '电子邮箱',
}
