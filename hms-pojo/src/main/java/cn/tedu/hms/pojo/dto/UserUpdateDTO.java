package cn.tedu.hms.pojo.dto;

import lombok.Data;

@Data
public class UserUpdateDTO {
    private String password;
    private String idCard;
    private String phone;
    private String email;
}
