drop table if exists hms_user;
create table hms_user
(
    id             bigint unsigned auto_increment primary key,
    username       varchar(50)                null comment '用户名',
    password       char(64)                   null comment '密码（密文）',
    id_card         char(20)                   null comment '身份证号',
    phone          varchar(50)                null comment '手机号码',
    email          varchar(50)                null comment '电子邮箱',
    last_login_ip  varchar(50)                null comment '最后登录IP地址（冗余）',
    login_count    int unsigned     default 0 null comment '累计登录次数（冗余）',
    gmt_last_login datetime                   null comment '最后登录时间（冗余）',
    gmt_create     datetime                   null comment '数据创建时间',
    gmt_modified   datetime                   null comment '数据最后修改时间',
    constraint email
        unique (email),
    constraint phone
        unique (phone)
) comment '用户信息表' charset = utf8mb4;

insert into hms_user
(username, password, idcard, phone)
values
('小明','123456','123456789123456789','12345678910'),
('小红','123456','123456789123456789','22345678910'),
('小黄','123456','123456789123456789','32345678910')

