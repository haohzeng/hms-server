package cn.tedu.hms.user.service;

import cn.tedu.hms.pojo.dto.UserDTO;

public interface IUserService {

    void addUser(UserDTO userDTO);

}
