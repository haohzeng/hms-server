package cn.tedu.hms.user.webapi.controller;

import cn.tedu.hms.common.web.JsonResult;
import cn.tedu.hms.pojo.dto.UserDTO;
import cn.tedu.hms.user.service.IUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
@Api(tags = "用户控制器")
public class UserController {

    @Autowired
    IUserService userService;

    @PostMapping("/addUser")
    @ApiOperation(value = "用户注册")
    public JsonResult<Void> addUser(UserDTO userDTO){
        userService.addUser(userDTO);
        return JsonResult.ok();
    }
}
