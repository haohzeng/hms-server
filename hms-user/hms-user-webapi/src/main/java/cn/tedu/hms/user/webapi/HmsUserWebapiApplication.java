package cn.tedu.hms.user.webapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HmsUserWebapiApplication {

    public static void main(String[] args) {
        SpringApplication.run(HmsUserWebapiApplication.class, args);
    }

}
