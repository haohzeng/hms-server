package cn.tedu.hms.user.webapi.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("cn.tedu.hms.user.webapi.mapper")
public class MybatisConfiguration {

}
