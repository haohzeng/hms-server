package cn.tedu.hms.user.webapi.mapper;

import cn.tedu.hms.pojo.dto.UserUpdateDTO;
import cn.tedu.hms.pojo.entity.User;
import cn.tedu.hms.pojo.vo.UserVO;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper {

    /**
     * 增加用户
     * @param user
     * @return
     */
    public int addUser(User user);

    /**
     * 根据id获取用户信息
     * @param id
     * @return
     */
    public UserVO getUserById(Long id);

    /**
     * 根据用户名获取用户信息
     * @param name
     * @return
     */
    public UserVO getUserByName(String name);

    /**
     *
     * @param userUpdateDTO
     * @return 返回修改生效的行数
     */
    int updatePersonalInfo(UserUpdateDTO userUpdateDTO);
}
