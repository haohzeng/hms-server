package cn.tedu.hms.user.webapi;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;
import java.sql.Connection;

@SpringBootTest
class HmsUserWebapiApplicationTests {

    @Autowired
    DataSource dataSource;

    @Test
    void contextLoads() {

    }

    @Test
    public void testConnection(){
        Assertions.assertDoesNotThrow(()->{
            Connection connection = dataSource.getConnection();
        });
    }


}
