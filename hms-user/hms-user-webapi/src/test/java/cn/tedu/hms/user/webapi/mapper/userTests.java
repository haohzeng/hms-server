package cn.tedu.hms.user.webapi.mapper;
import java.time.LocalDateTime;

import cn.tedu.hms.pojo.entity.User;
import cn.tedu.hms.pojo.vo.UserVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class userTests {
    @Autowired
    UserMapper mapper;


    @Test
    public void testAddUser(){
        User user = new User();
        user.setId(10L);
        user.setUsername("用户添加");
        user.setPassword("123456");
        user.setIdCard("12321212131");
        user.setPhone("123124334353");
        user.setEmail("123124235");

        int rows = mapper.addUser(user);
        System.out.println("rows = " + rows);
    }

    @Test
    public void testGetById(){
        Long id = 1L;
        UserVO user = mapper.getUserById(id);
        System.out.println("user = " + user);
    }
}
