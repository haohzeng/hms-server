package cn.tedu.hms.common.ex;

import cn.tedu.hms.common.web.State;

public class ServiceException extends RuntimeException{

    private State state;

    public ServiceException(State state,String message) {
        super(message);
        if (state == null) {
            throw new RuntimeException("ServiceException异常 state状态码为空 应该加入状态码");
        }
        this.state = state;
    }

    public State getState(){
        return this.state;
    }
}
