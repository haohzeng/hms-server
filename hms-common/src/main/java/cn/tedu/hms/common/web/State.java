package cn.tedu.hms.common.web;

public enum State {

    OK(20000),
    ERR_BAD_REQUEST(40000),             //4:客户端 0:请求
    ERR_ERROR_USERNAME(40100),          //客户端提交的用户名错误
    ERR_ERROR_PASSWORD(40101),          //客户端提交的密码错误



    ERR_JWT_EXPIRED(40900),//jwt过期
    ERR_JWT_MALFORMED(40901),//jwt错误
    ERR_JWT_SIGNATURE(40902),//jwt签名错误

    ERR_INSERT(50000),                  //5:服务端 插入数据错误
    ERR_UPDATE(50001),                  //5:服务端 更改数据错误

    ERR_INTERNAL_SERVER_ERROP(50100);   //未知错误

    private Integer value;

    State(Integer value){
        this.value = value;
    }

    public Integer getValue(){
        return value;
    }
}
