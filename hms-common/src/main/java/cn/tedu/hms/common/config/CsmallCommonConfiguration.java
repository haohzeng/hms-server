package cn.tedu.hms.common.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("cn.tedu.hms.common.ex.handler")
public class CsmallCommonConfiguration {
}
