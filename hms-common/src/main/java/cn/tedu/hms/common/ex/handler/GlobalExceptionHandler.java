package cn.tedu.hms.common.ex.handler;

import cn.tedu.hms.common.ex.ServiceException;
import cn.tedu.hms.common.web.JsonResult;
import cn.tedu.hms.common.web.State;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler({ServiceException.class})
    public JsonResult<Void> handleServiceException(ServiceException ex){
        return JsonResult.fail(ex.getState(),ex.getMessage());
    }

    @ExceptionHandler(BindException.class)
    public JsonResult<Void> handleBindException(BindException ex){

        //有多个错误的时候 拼接起来
        List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
        StringBuilder stringBuilder = new StringBuilder();
        for (FieldError fieldError : fieldErrors) {
            stringBuilder.append(";");
            stringBuilder.append(fieldError.getDefaultMessage());
        }
        //stringBuffuer=";error1;error2;error3..."
        String message = stringBuilder.substring(1);
        //message="error1;error2;error3"
        return JsonResult.fail(State.ERR_BAD_REQUEST,message);
    }
}
