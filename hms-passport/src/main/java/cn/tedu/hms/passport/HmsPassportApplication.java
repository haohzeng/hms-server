package cn.tedu.hms.passport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HmsPassportApplication {

    public static void main(String[] args) {
        SpringApplication.run(HmsPassportApplication.class, args);
    }

}
